// SCA.h : main header file for the SCA DLL
//

#ifndef _SCA_H_
#define _SCA_H_


#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <vector>
#include "IoStream"
using namespace std;

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitSuite(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);

// Open a document view
extern "C" AFX_EXT_API void OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);

void WriteToLog(CString message2, bool error/*=false*/);

#define _debug


#endif
